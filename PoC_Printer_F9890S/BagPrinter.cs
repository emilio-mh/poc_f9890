﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace PoC_Printer_F9890S
{
    // The parameter of the Gpp3GetPrinterStatus
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
    public struct GppStatusMonitor
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 221)]
        public string printerName;          // +0000h The printer name
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 38)]
        public byte[] reserved1;            // +01BAh
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 8)]
        public string deviceSerialNumber;   // +01E0h The device serial number
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 6)]
        public string firmwareVersion;      // +01F0h The firmware version
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] reserved2;            // +01FCh
        public int firmwareErrorCode;    // +0200h The firmware error code
        public ushort firmwareStatus;       // +0204h The firmware status
        public ushort sensorCondition;      // +0206h The sensor condition
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string userName;             // +0208h The user name in printing
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 1024)]
        public string documentName;         // +0408h The document name in printing 
        public int jobId;                // +0C08h The print job ID in printing
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
        public byte[] reserved3;            // +0C0C
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 256)]
        public string serverHostName;       // +0C10h The printer server host name
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 64)]
        public string serverPortName;       // +0E10h The printer server port name
    }
    public class BagPrinter
    {

        // The functions of the F9890 Window Driver API.
        [DllImport("C:\\Users\\webmaster\\Downloads\\poc_f9890-master\\poc_f9890-master\\PoC_Printer_F9890S\\F9890API.dll", SetLastError = true, EntryPoint = "Gpp3GetPrinterStatus", CharSet = CharSet.Unicode)]
        private static extern bool Gpp3GetPrinterStatus(System.String printerName, ref GppStatusMonitor status);
        [DllImport("C:\\Users\\webmaster\\Downloads\\poc_f9890-master\\poc_f9890-master\\PoC_Printer_F9890S\\F9890API.dll", SetLastError = true, EntryPoint = "Gpp3GetPrinterLog", CharSet = CharSet.Unicode)]
        private static extern bool Gpp3GetPrinterLog(System.String printerName);
        [DllImport("C:\\Users\\webmaster\\Downloads\\poc_f9890-master\\poc_f9890-master\\PoC_Printer_F9890S\\F9890API.dll", SetLastError = true, EntryPoint = "Gpp3ResetPrinter", CharSet = CharSet.Unicode)]
        private static extern bool Gpp3ResetPrinter(System.String printerName);
        [DllImport("C:\\Users\\webmaster\\Downloads\\poc_f9890-master\\poc_f9890-master\\PoC_Printer_F9890S\\F9890API.dll", SetLastError = true, EntryPoint = "Gpp3SetRfidDataPath", CharSet = CharSet.Unicode)]
        private static extern bool Gpp3SetRfidDataPath(System.IntPtr devMode, System.String epcDataPath, System.String userDataPath);
        [DllImport("C:\\Users\\webmaster\\Downloads\\poc_f9890-master\\poc_f9890-master\\PoC_Printer_F9890S\\F9890API.dll", SetLastError = true, EntryPoint = "Gpp3SetDefaultRfidDataPath", CharSet = CharSet.Unicode)]
        private static extern bool Gpp3SetDefaultRfidDataPath(System.String printerName, System.String epcDataPath, System.String userDataPath);
        [DllImport("C:\\Users\\webmaster\\Downloads\\poc_f9890-master\\poc_f9890-master\\PoC_Printer_F9890S\\F9890API.dll", SetLastError = true, EntryPoint = "Gpp3WriteRfid", CharSet = CharSet.Unicode)]
        private static extern bool Gpp3WriteRfid(System.String printerName, byte bankNumber, [In, Out] byte[] dataBuffer, uint dataLength);
        [DllImport("C:\\Users\\webmaster\\Downloads\\poc_f9890-master\\poc_f9890-master\\PoC_Printer_F9890S\\F9890API.dll", SetLastError = true, EntryPoint = "Gpp3ReadRfid", CharSet = CharSet.Unicode)]
        private static extern bool Gpp3ReadRfid(System.String printerName, byte bankNumber, [In, Out] byte[] dataBuffer, ref uint dataLength);
        [DllImport("C:\\Users\\webmaster\\Downloads\\poc_f9890-master\\poc_f9890-master\\PoC_Printer_F9890S\\F9890API.dll", SetLastError = true, EntryPoint = "Gpp3LockRfid", CharSet = CharSet.Unicode)]
        private static extern bool Gpp3LockRfid(System.String printerName, byte lockArea, byte lockMode, [In, Out] byte[] accessPassword);

        private GppStatusMonitor _monitor;
        public string printerName { get; }

        public BagPrinter(string printerName)
        {
            _monitor = new GppStatusMonitor();
            this.printerName = printerName;
        }

        public bool GetStatus ()
        {
            var status = Gpp3GetPrinterStatus(printerName, ref _monitor);
            return status;
        }

        public byte[] ReadRfid()
        {
            byte[] dataBuffer = new byte[256];
            uint dataLength = 0;
            bool success = Gpp3ReadRfid(printerName, (byte)1, dataBuffer, ref dataLength);
            if (!success) return null;
            return dataBuffer;
        }

        public void Print(object sender, PrintPageEventArgs e)
        {
            // We only print on one page.
            e.HasMorePages = false;

            // Prints an image on the current page. 
            try
            {
                // Reads an image file.
                // Please rewrite the following path string, to the file that you want to print.
                using (System.Drawing.Image image = System.Drawing.Image.FromFile("C:\\Users\\Emilio Martinez\\Downloads\\qr-code.png"))
                {

                    // Renders the image.
                    e.Graphics.PageUnit = GraphicsUnit.Pixel;
                    e.Graphics.DrawImage(image, 0, 0, image.Width, image.Height);

                    // If You want to render with scaling to fill the printable area, 
                    // You can use the following code. 
                    //e.Graphics.DrawImage(image, e.Graphics.VisibleClipBounds);

                }
            }
            catch { }

            // Prints a string on the current page.
            using (Font font = new Font("Arial", 12, FontStyle.Regular, GraphicsUnit.Point))
            {
                e.Graphics.DrawString("Sample", font, Brushes.Black, e.Graphics.VisibleClipBounds);
            }
        }
    }
}
