﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Drawing.Printing;
using System.Management;
using System.Net.NetworkInformation;
using System.Printing;

namespace PoC_Printer_F9890S
{
    internal class Program
    {

        // The functions of the Windows API.
        [DllImport("kernel32.dll")]
        private static extern System.IntPtr GlobalLock(System.IntPtr hMem);
        [DllImport("kernel32.dll")]
        private static extern bool GlobalUnlock(System.IntPtr hMem);
        [DllImport("kernel32.dll")]
        private static extern System.IntPtr GlobalFree(System.IntPtr hMem);

        static void Main(string[] args)
        {
            var printDoc = new PrintDocument();
            printDoc.PrinterSettings.PrinterName = "Fujitsu Frontech F9890";
            var printer = new BagPrinter(printDoc.PrinterSettings.PrinterName);

            foreach (string printerName in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            {
                Console.WriteLine(printerName);
            }
            //var status = printer.GetStatus();
            var format = "Label (B-TAG)";
            var sizes = printDoc.PrinterSettings.PaperSizes;
            printDoc.DefaultPageSettings.PaperSize = sizes[2];
            //printDoc.DefaultPageSettings.PaperSize = new PaperSize
            //{
            //    PaperName="Label (B-TAG) Kind=Custom",
            //    Height=2000,
            //    Width=213
            //};
            printDoc.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(printer.Print);
            //printDoc.PrinterSettings.PrinterName = "sadsa";
            printDoc.Print();

        }
    }
}
